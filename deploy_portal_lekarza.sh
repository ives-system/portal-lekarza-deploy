echo "Log to server, check screen -ls. Switch to screen -r {pid-screen} kill proces"
echo "Clean up..."
rm -rf braster-portal-lekarza
git clone git@bitbucket.org:ives-system/braster-portal-lekarza.git
cd braster-portal-lekarza
git checkout origin/prod
cp ../config.js config.js

echo "Create zip file"
zip -r portal-lekarza.zip * .[^.]*

echo "Kill server"
ssh braster-portal-prod "cd /var/www/portal-lekarza;rm -rf *"

echo "Deploy portal lekarza client"
scp -r portal-lekarza.zip braster-portal-prod:/var/www/portal-lekarza/

echo "Extracting and start"
ssh braster-portal-prod "cd /var/www/portal-lekarza; unzip -o portal-lekarza.zip;npm install;npm run build"

echo "Log to server, check screen -ls. Switch to screen -r {pid-screen} and run 'npm run dev'. After run detached screen Ctrl + D -> Ctrl + A";
