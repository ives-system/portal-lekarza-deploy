module.exports = {
    zuulUrl: 'https://backend.braster.eu',
    viewerUrl: 'https://wizualizator.braster.eu/public/braster-viewer/popup.htm',
    autoLogoutTimeInSeconds: 1800
};
